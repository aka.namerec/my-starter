# IntProp SandBox Starter

## General Notes
Git Submodules are used extensively, so be adviced to use
`git clone --recurse-submodules -j8` instead of simple `git clone`.
Chances are, you'll need to edit some of submodules — feel free to branch 'em
and redirect the sandbox's submodule link accordingly.

## Starter Notes
Starter Sandbox Repo is to be forked when starting a new sandbox project.
After forking, add required sandboxes as git submodules, like that:
```
git submodule add git@gitlab.com:intprop.lib.backend.plugs/fias.plug.git backend/fias # for plugs
git submodule add git@gitlab.com:intprop.lib.backend/companies.git backend/companies # for actual modules
```
...and start developing. Normally, the sandbox itself will work as a test project only, so
all the real developement will happen in module repos — feel free to fork 'em.

Some submodules are added already: `common`, `users`, `companies.plug`.
Replace `companies.plug` with `companies` in case you'll need full-scale companies.

## Code organization notes
Please follow the approach used in `models` of existing modules. It's Java-like, basically.
I know many people hate Java, and I tend to dislike it myself, but for code organization
the approach is kinda optimal.

## Dev Setup

```
# git clone --recurse-submodules -j8 ...trials.git && cd trials/backend
python3 -m venv venv
. venv/bin/activate
pip install -r requirements.txt
./manage.py migrate
./manage.py loaddata users/fixtures/root@example.com.json
./scripts/tasks/compile_translations.sh
./manage.py runserver
```

## Default Superuser Credentials
root@example.com / password
