#!/bin/bash

printf "<setting up development environment>\n"

./scripts/tasks/setup_venv.sh
./scripts/tasks/install_common_dependencies.sh
./scripts/tasks/copy_dev_example_local_settings.sh
./scripts/tasks/compile_translations.sh
./scripts/tasks/migrate_database.sh
./scripts/tasks/load_fixtures.sh

printf "</setting up development environment>\n"
