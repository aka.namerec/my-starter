#!/bin/bash

printf "<compiling translations>\n"

printf "\t<activating venv>\n"
. venv/bin/activate
printf "\t</activating venv>\n"

app_names=(common users companies)
# trade intellectual_properties government_institutions purchases plainter trials

for i in "${app_names[@]}"
do
    printf "\t<$i>\n"
    cd $i
    ../manage.py compilemessages -l ru
    cd ..
    printf "\t</$i>\n"
done

printf "</compiling translations>\n"
