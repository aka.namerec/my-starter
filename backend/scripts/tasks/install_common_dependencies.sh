#!/bin/bash

printf "<installing common dependencies>\n"

printf "\t<activating venv>\n"
. venv/bin/activate
printf "\t</activating venv>\n"

printf "\t<installing dependencies from requirements.txt>\n"
pip install -r requirements.txt
printf "\t</installing dependencies from requirements.txt>\n"

printf "</installing common dependencies>\n"
