#!/bin/bash

printf "<setting up venv>\n"

printf "\t<venv itself>\n"
python3 -m venv venv
. venv/bin/activate
printf "\t</venv itself>\n"

printf "\t<upgrading pip>\n"
pip install --upgrade pip
printf "\t</upgrading pip>\n"

printf "\t<installing wheel>\n"
pip install wheel
printf "\t</installing wheel>\n"

printf "</setting up venv>\n"