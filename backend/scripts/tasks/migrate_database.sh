#!/bin/bash

printf "<migrating the database>\n"

printf "\t<activating venv>\n"
. venv/bin/activate
printf "\t</activating venv>\n"

printf "\t<running ./manage.py migrate>\n"
./manage.py migrate
printf "\t</running ./manage.py migrate>\n"

printf "</migrating the database>\n"
