#!/bin/bash

printf "<loading fixtures>\n"

printf "\t<activating venv>\n"
. venv/bin/activate
printf "\t</activating venv>\n"

printf "\t<loading fixtures/superusers.json>\n"
./manage.py loaddata fixtures/superusers.json
printf "\t</loading fixtures/superusers.json>\n"

printf "</loading fixtures>\n"
