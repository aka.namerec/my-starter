#!/bin/bash

printf "<extracting translations>\n"

printf "\t<activating venv>\n"
. venv/bin/activate
printf "\t</activating venv>\n"

app_names=(common users companies)
# trade intellectual_properties government_institutions purchases plainter trials)

for i in "${app_names[@]}"
do
    printf "\t<$i>\n"
    cd $i
    mkdir -p locale
    ../manage.py makemessages -l ru
    cd ..
    printf "\t</$i>\n"
done

printf "</extracting translations>\n"
