import re

camelcase_to_underscore = lambda str:\
    re.sub('(((?<=[a-z])[A-Z])|([A-Z](?![A-Z]|$)))', '_\\1', str).lower().strip('_')

camelcase_to_dash = lambda str:\
    re.sub('(((?<=[a-z])[A-Z])|([A-Z](?![A-Z]|$)))', '-\\1', str).lower().strip('-')
