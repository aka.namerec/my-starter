from django.contrib import admin


class AbstractBaseModelAdmin(admin.ModelAdmin):
    list_display = ('pk', 'updated_on', 'updated_by', 'created_on', 'created_by',)
