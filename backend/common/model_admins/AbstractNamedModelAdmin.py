from django.contrib import admin


class AbstractNamedModelAdmin(admin.ModelAdmin):
    list_display = ('pk', 'name', 'updated_on', 'updated_by', 'created_on', 'created_by',)
    search_fields = ('name',)
