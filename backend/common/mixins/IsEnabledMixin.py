from django.db import models
from django.utils.translation import ugettext_lazy as _


__all__ = ('IsEnabledMixin',)

class IsEnabledMixin(models.Model):
    class Meta:
        abstract = True

    is_enabled = models.BooleanField(verbose_name=_('is enabled'), default=True)
