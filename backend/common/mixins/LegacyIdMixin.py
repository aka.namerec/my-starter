from django.db import models
from django.utils.translation import ugettext_lazy as _


__all__ = ('LegacyIdMixin',)


class LegacyIdMixin(models.Model):
    class Meta:
        abstract = True

    legacy_id = models.IntegerField(verbose_name=_('legacy ID'),
        null=True, blank=True, unique=True, editable=False)
