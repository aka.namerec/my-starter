from django.db import models
from django.utils.translation import ugettext_lazy as _


__all__ = ('LegacyCodeMixin',)


class LegacyCodeMixin(models.Model):
    class Meta:
        abstract = True

    legacy_code = models.IntegerField(verbose_name=_('legacy code'),
        null=True, blank=True, unique=True, editable=False)
