from django.db import models
from .NameMixin import NameMixin
from django.utils.translation import ugettext_lazy as _


class UniqueNameMixin(NameMixin):
    class Meta:
        abstract = True

    name = models.CharField(
        max_length=100,
        verbose_name=_('name'),
        null=True,
        blank=True,
        unique=True,
        db_index=True
    )
