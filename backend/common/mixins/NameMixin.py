from django.db import models
from django.utils.translation import ugettext_lazy as _


class NameMixin(models.Model):
    class Meta:
        abstract = True

    name = models.CharField(
        max_length=100,
        verbose_name=_('name'),
        null=True,
        blank=True,
        db_index=True
    )

    def __str__(self):
        return self.name

    # autocomplete_query_field = 'name'
