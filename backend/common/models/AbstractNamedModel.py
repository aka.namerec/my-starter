from django.db import models
from ..mixins import NameMixin
from .AbstractBaseModel import AbstractBaseModel


class AbstractNamedUniquelyModel(AbstractBaseModel, NameMixin):
    class Meta:
        abstract = True
