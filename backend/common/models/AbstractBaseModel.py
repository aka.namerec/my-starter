from ..mixins import TimestampMixin
# from ..mixins import WhoDidItMixin


__all__ = ('AbstractBaseModel',)


class AbstractBaseModel(TimestampMixin):
    class Meta(TimestampMixin.Meta):
        abstract = True
