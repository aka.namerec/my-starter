from django.db import models
from ..mixins import UniqueNameMixin
from .AbstractBaseModel import AbstractBaseModel


class AbstractNamedUniquelyModel(AbstractBaseModel, UniqueNameMixin):
    class Meta:
        abstract = True
