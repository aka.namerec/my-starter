from rest_framework.permissions import BasePermission


class IsAuthorizedPlainterUser(BasePermission):
    def has_permission(self, request, view):
        return request.user and request.user.is_authenticated and \
            (request.user.is_superuser or request.user.is_plainter_user)
