# Generated by Django 2.1.7 on 2019-03-06 10:25

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('users', '0002_auto_20190306_1324'),
    ]

    operations = [
        migrations.AddField(
            model_name='user',
            name='is_purchase_footmen',
            field=models.BooleanField(default=False, verbose_name='is purchase footmen'),
        ),
        migrations.AddField(
            model_name='user',
            name='is_purchase_manager',
            field=models.BooleanField(default=False, verbose_name='is purchase manager'),
        ),
        migrations.AddField(
            model_name='user',
            name='is_purchase_moderator',
            field=models.BooleanField(default=False, verbose_name='is purchase moderator'),
        ),
    ]
