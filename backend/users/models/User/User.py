from django.utils.translation import ugettext_lazy as _
from django.contrib.auth.models import AbstractUser
from django.db import models
from common.mixins import TimestampMixin


from .UserManager import UserManager


class User(AbstractUser, TimestampMixin):
    class Meta(AbstractUser.Meta):
        default_related_name = 'users'

    username = None
    email = models.EmailField(_('email address'), unique=True)

    objects = UserManager()

    USERNAME_FIELD = 'email'
    REQUIRED_FIELDS = []

    first_name = None
    last_name = None

    person = models.ForeignKey(
        verbose_name=_('person'),
        to='companies.Person',
        null=True,
        blank=True,
        on_delete=models.PROTECT,
    )

    is_purchase_footmen = models.BooleanField(_('is purchase footmen'), default=False)
    is_purchase_manager = models.BooleanField(_('is purchase manager'), default=False)
    is_purchase_moderator = models.BooleanField(_('is purchase moderator'), default=False)

    is_storefront_user = models.BooleanField(_('is storefront user'), default=False)
    is_plainter_user = models.BooleanField(_('is plainter user'), default=False)
    is_erlt_user = models.BooleanField(_('is erlt user'), default=False)
    is_flown_user = models.BooleanField(_('is flown user'), default=False)
    is_webseer_user = models.BooleanField(_('is webseer user'), default=False)
