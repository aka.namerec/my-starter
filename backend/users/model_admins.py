from .models.User.UserAdmin import UserAdmin


__all__ = (
    'UserAdmin',
    'register_model_admins',
)


from .models.User import User


def register_model_admins (site):
    site.register(User, UserAdmin)
