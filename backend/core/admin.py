from django.contrib.admin import site

apps_to_register = [
   'users',
   'companies',
#    'intellectual_properties',
#    'government_institutions',
#    'trade',
#    'purchases',
#    'plainter',
#    'trials',
]

for app_name in apps_to_register:
    m = __import__(app_name + '.model_admins', fromlist=[None])
    m.register_model_admins(site)
