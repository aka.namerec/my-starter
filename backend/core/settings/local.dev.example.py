from .common import *

DEBUG = True
ALLOWED_HOSTS = ('localhost',)
SECRET_KEY = 'your_secret_key-;sdjkgh'

from .utils import BASE_DIR, os

DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.sqlite3',
        'NAME': os.path.join(BASE_DIR, 'db.sqlite3'),
    }
}