from .default import *

LANGUAGE_CODE = 'ru-ru'
TIME_ZONE = 'Europe/Moscow'

INSTALLED_APPS += (
    'common',
    'users', 
    'companies',
    'core',
)

MIDDLEWARE += ('common.middleware.WhoDidItMiddleware',)

AUTH_USER_MODEL = 'users.User'
