import os

# Build paths inside the project like this: os.path.join(BASE_DIR, ...)
BASE_DIR = os.path.dirname(                 # project dir
    os.path.dirname(                        # core
        os.path.dirname(                    # settings/
            os.path.abspath(__file__)       # utils.py
        )
    )
)
