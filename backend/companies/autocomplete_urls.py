from django.urls import path
from .models.IncorporationType.IncorporationTypeAutocompleteView import IncorporationTypeAutocompleteView
from .models.Company.CompanyAutocompleteView import CompanyAutocompleteView
from .models.Company.Rightholder.RightholderAutocompleteView import RightholderAutocompleteView
from .models.Person.PersonAutocompleteView import PersonAutocompleteView

urlpatterns = [
    path('incorporation-type/', IncorporationTypeAutocompleteView.as_view(),
        name='incorporation-type-autocomplete',),
    path('company/', CompanyAutocompleteView.as_view(), name='company-autocomplete',),
    path('company/rightholder/', RightholderAutocompleteView.as_view(), name='rightholder-autocomplete',),
    path('person/', PersonAutocompleteView.as_view(), name='person-autocomplete',),
]
