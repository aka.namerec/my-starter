from django.utils.translation import ugettext_lazy as _
from .Partner import Partner
from ..Company import CompanySerializer


__all__ = ('PartnerSerializer',)


class PartnerSerializer(CompanySerializer):
    class Meta(CompanySerializer.Meta):
        model = Partner
