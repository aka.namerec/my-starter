from django.utils.translation import ugettext_lazy as _
from .Partner import Partner
from .PartnerSerializer import PartnerSerializer


__all__ = ('PartnerViewSet',)


class PartnerViewSet(CompanyViewSet):
    queryset = Partner.objects.all()
    serializer_class = PartnerSerializer
