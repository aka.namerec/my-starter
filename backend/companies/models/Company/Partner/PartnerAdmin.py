from ..CompanyAdmin import CompanyAdmin


__all__ = ('PartnerAdmin',)


class PartnerAdmin(CompanyAdmin):
    list_filter = ( 'incorporation_type', )
