from django.db.models import Manager


__all__ = ('PartnerManager',)


class PartnerManager(Manager):
    def get_queryset(self):
        return super(PartnerManager, self).get_queryset().filter(is_partner=True)

    def create(self, **kwargs):
        kwargs.update({'is_partner': True})
        return super(PartnerManager, self).create(**kwargs)
