from django.utils.translation import ugettext_lazy as _
from ..Company import Company
from .PartnerManager import PartnerManager


__all__ = ('Partner',)


class Partner(Company):
    objects = PartnerManager()
    class Meta:
        proxy = True
        verbose_name = _('partner')
        verbose_name_plural = _('partners')

    def get_absolute_url(self):
        return '/ui/plainter/partners/%d/' % (self.id)

    def save(self, *args, **kwargs):
        if not self.id:
            self.is_partner = True
        return super().save(*args, **kwargs)
