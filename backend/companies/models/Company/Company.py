from django.core.exceptions import ValidationError
from django.db import models
from django.utils.translation import ugettext_lazy as _
# from fias.models import FIASStreetAddress

from common.mixins import LegacyIdMixin
from common.models import AbstractBaseModel

__all__ = ('Company',)


# class Company(AbstractBaseModel, LegacyIdMixin, FIASStreetAddress):
class Company(AbstractBaseModel, LegacyIdMixin):
    class Meta:
        verbose_name = _('company')
        verbose_name_plural = _('companies')

    incorporation_type = models.ForeignKey(to='IncorporationType', verbose_name=_('incorporation type'),
                                           on_delete=models.PROTECT, null=True, blank=True)

    individual_enterpreneur_person = models.OneToOneField(to='Person',
                                                          verbose_name=_('individual enterpreneur person'),
                                                          null=True, blank=True, on_delete=models.PROTECT,
                                                          related_name='individual_enterprise',
                                                          )

    full_name = models.CharField(max_length=255, verbose_name=_('full_name'),
                                 help_text=_('auto-filled for induvidual enterpreneurs'))
    short_name = models.CharField(max_length=255, verbose_name=_('short name'),
                                  help_text=_('auto-filled for induvidual enterpreneurs'), blank=True, null=True)

    inn = models.CharField(max_length=32, verbose_name=_('INN'), unique=True, null=True, blank=True)
    ogrn = models.CharField(max_length=32, verbose_name=_('OGRN'), unique=True, null=True, blank=True)
    registration_date = models.DateField(verbose_name=_('registration date'), null=True, blank=True)

    website_url = models.URLField(max_length=255, verbose_name=_('website_url'), blank=True, null=True)
    phone = models.CharField(max_length=255, verbose_name=_('phone'), blank=True, null=True)
    email = models.EmailField(max_length=255, verbose_name=_('email'), blank=True, null=True)

    is_rightholder = models.BooleanField(verbose_name=_('is rightholder'), default=False)
    is_partner = models.BooleanField(verbose_name=_('is partner'), default=False)
    is_violator = models.BooleanField(verbose_name=_('is violator'), default=False)

    comment = models.TextField(verbose_name=_('comment'), null=True, blank=True)

    payment_details = models.TextField(verbose_name=_('payment details'), null=True, blank=True)

    # normally auto-filled by FIAS
    # full_address = models.CharField(_('legal address'), max_length=255, blank=True, editable=False)
    full_address = models.CharField(_('legal address'), max_length=255, blank=True)
    short_address = models.CharField(_('short address'), max_length=255, blank=False, null=False)

    foreign_legal_address = models.CharField(_('foreign legal address'), max_length=255, null=True, blank=True)

    def __str__(self):
        return self.name

    @property
    def name(self):
        return ("%s «%s»" % (self.incorporation_type.abbreviation, self.full_name or self.short_name)) \
            if self.incorporation_type_id else (self.short_name or self.full_name)

    # individual enterpreneurs should always have related personal records set
    def clean(self):
        if self.incorporation_type and self.incorporation_type.abbreviation == 'ИП':
            if not self.individual_enterpreneur_person:
                raise ValidationError(
                    _('Personal information is obligatory for individual enterpreneurs'))
            elif not (
                    self.individual_enterpreneur_person.last_name and
                    self.individual_enterpreneur_person.first_name and
                    self.individual_enterpreneur_person.middle_name
            ):
                raise ValidationError(_('Specified person must have all three names set'))
            elif not self.individual_enterpreneur_person.inn:
                raise ValidationError(_('Specified person must have INN set'))

    # if individual enterpreneurs's personal record is present, copy data from there
    def save(self, *args, **kwargs):
        if self.incorporation_type and self.incorporation_type.abbreviation == 'ИП' \
                and self.individual_enterpreneur_person:
            self.full_name = '%s' % self.individual_enterpreneur_person.full_name
            self.short_name = '%s %s.%s.' % (
                self.individual_enterpreneur_person.last_name,
                self.individual_enterpreneur_person.first_name[0]
                if self.individual_enterpreneur_person.first_name else '',
                self.individual_enterpreneur_person.middle_name[0]
                if self.individual_enterpreneur_person.middle_name else ''
            )
            self.short_name = '.'.join([n for n in self.short_name.split('.') if n]) + '.'
            self.inn = self.individual_enterpreneur_person.inn
        super().save(*args, **kwargs)

    # # TODO: review, probably get rid of it
    # @property
    # def cases_as_defendant(self):
    #     from plainter.models import Case
    #     from purchases.models import Purchase
    #     purchases = Purchase.objects.filter(store__in=self.stores.all())
    #     cases = Case.objects.filter(purchase__in=purchases)
    #     return cases
