from dynamic_rest.serializers import DynamicModelSerializer, DynamicRelationField

from .incorporation_type import IncorporationTypeSerializer
from .Company import Company


class CompanySerializer(DynamicModelSerializer):
    class Meta:
        model = Company
        fields = (
            'id', 'name',
            'incorporation_type',
            'full_name', 'short_name',
            'inn', 'ogrn',
            'registration_date',
            'website_url', 'phone', 'email',
            'is_rightholder', 'comment',
        )

    incorporation_type = DynamicRelationField(IncorporationTypeSerializer)
