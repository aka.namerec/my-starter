from dal import autocomplete
from .Company import Company


class CompanyAutocompleteView(autocomplete.Select2QuerySetView):
    def get_queryset(self):
        qs = Company.objects.all()

        if self.q:
            qs = qs.filter(name__istartswith=self.q)

        return qs
