from django.utils.translation import ugettext_lazy as _
from ..Company import Company
from .ViolatorManager import ViolatorManager


__all__ = ('Violator',)


class Violator(Company):
    objects = ViolatorManager()
    class Meta:
        proxy = True
        verbose_name = _('violator')
        verbose_name_plural = _('violators')

    def get_absolute_url(self):
        return '/ui/plainter/violators/%d/' % (self.id)

    def save(self, *args, **kwargs):
        if not self.id:
            self.is_violator = True
        return super().save(*args, **kwargs)

    def get_edit_url(self):
        return self.get_absolute_url() + 'edit/'
