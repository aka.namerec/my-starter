from ..CompanyViewSet import CompanyViewSet
from .Violator import Violator


__all__ = ('ViolatorViewSet',)


class ViolatorViewSet(CompanyViewSet):
    queryset = Violator.objects.all()
    serializer_class = ViolatorSerializer
