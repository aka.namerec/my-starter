from django.db.models import Manager


__all__ = ('ViolatorManager',)


class ViolatorManager(Manager):
    def get_queryset(self):
        return super().get_queryset().filter(is_violator=True)

    def create(self, **kwargs):
        kwargs.update({'is_violator': True})
        return super().create(**kwargs)
