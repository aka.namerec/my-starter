from .Violator import Violator
from ..CompanySerializer import CompanySerializer


__all__ = ('ViolatorSerializer',)


class ViolatorSerializer(CompanySerializer):
    class Meta(CompanySerializer.Meta):
        model = Violator
