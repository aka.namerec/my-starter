from ..CompanyAdmin import CompanyAdmin


__all__ = ('ViolatorAdmin',)


class ViolatorAdmin(CompanyAdmin):
    list_filter = ( 'incorporation_type', )
