from .Rightholder import Rightholder
from .RightholderSerializer import RightholderSerializer


class RightholderViewSet(CompanyViewSet):
    queryset = Rightholder.objects.all()
    serializer_class = RightholderSerializer
