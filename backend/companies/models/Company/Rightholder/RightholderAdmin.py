from ..CompanyAdmin import CompanyAdmin
# from intellectual_properties.models.IntellectualProperty.IntellectualPropertyAdminInline\
#     import IntellectualPropertyAdminInline


class RightholderAdmin(CompanyAdmin):
    # inlines = (IntellectualPropertyAdminInline,)
    list_filter = ( 'incorporation_type', )
