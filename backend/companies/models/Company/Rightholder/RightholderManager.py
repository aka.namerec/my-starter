from django.db.models import Manager
from django.utils.translation import ugettext_lazy as _


__all__ = ('RightholderManager',)

class RightholderManager(Manager):
    def get_queryset(self):
        return super(RightholderManager, self).get_queryset().filter(is_rightholder=True)

    def create(self, **kwargs):
        kwargs.update({'is_rightholder': True})
        return super(RightholderManager, self).create(**kwargs)
