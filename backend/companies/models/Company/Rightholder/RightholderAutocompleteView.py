from dal import autocomplete
from .Rightholder import Rightholder


class RightholderAutocompleteView(autocomplete.Select2QuerySetView):
    def get_queryset(self):
        qs = Rightholder.objects.all()

        if self.q:
            qs = qs.filter(name__istartswith=self.q)

        return qs
