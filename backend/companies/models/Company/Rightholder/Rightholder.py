from django.utils.translation import ugettext_lazy as _
from .RightholderManager import RightholderManager
from ..Company import Company


class Rightholder(Company):
    objects = RightholderManager()
    class Meta:
        proxy = True
        verbose_name = _('rightholder')
        verbose_name_plural = _('rightholders')

    def get_absolute_url(self):
        return '/ui/plainter/rightholders/%d/' % (self.id)

    def save(self, *args, **kwargs):
        if not self.id:
            self.is_rightholder = True
        return super().save(*args, **kwargs)
