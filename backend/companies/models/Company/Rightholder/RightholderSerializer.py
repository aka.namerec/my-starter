from .Rightholder import Rightholder
from ..CompanySerializer import CompanySerializer


class RightholderSerializer(CompanySerializer):
    class Meta(CompanySerializer.Meta):
        model = Rightholder
