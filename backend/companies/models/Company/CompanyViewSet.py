from dynamic_rest.viewsets import DynamicModelViewSet
from .Company import Company
from .CompanySerializer import CompanySerializer


class CompanyViewSet(DynamicModelViewSet):
    queryset = Company.objects.all()
    serializer_class = CompanySerializer
