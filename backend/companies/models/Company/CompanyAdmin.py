from django.urls import reverse
from typing import List

from django.conf.urls import url
from django.contrib import admin
from django.contrib.messages import success
from django.core.handlers.wsgi import WSGIRequest
from django.http import HttpResponse, HttpResponseRedirect
from django.utils.translation import ugettext_lazy as _

# from ..IncorporationType.IncorporationTypeAutocompleteFilter import IncorporationTypeAutocompleteFilter


__all__ = ('CompanyAdmin',)


class CompanyAdmin(admin.ModelAdmin):
    list_display = (
        'id', '__str__',
        'is_rightholder', 'is_violator', 'is_partner',
        'incorporation_type',
        'full_name', 'short_name',
        'inn', 'ogrn',
        'registration_date',
        'website_url', 'phone', 'email',
        'comment',
        'payment_details',
    )
    list_display_links = ('id', 'full_name',)
    list_filter = (
        # IncorporationTypeAutocompleteFilter,
        'incorporation_type',
        'is_rightholder', 'is_violator', 'is_partner',
    )
    search_fields = ('full_name', 'short_name', 'phone', 'website_url', 'email', 'comment', 'inn', 'ogrn',)

    # autocomplete_fields = ('incorporation_type',)

    class Media:
        extend = True

    readonly_fields = (
        'name',
        # 'full_address', 'short_address', 'postal_code',
        'legacy_id',
        # 'federation_subject', 'settlement', 'postal_code',
        'created', 'updated',
    )
    autocomplete_fields = ('individual_enterpreneur_person',)
    fieldsets = (
        (None, {
            'fields': (
                'name', 'full_name', 'short_name', 'incorporation_type', 'individual_enterpreneur_person', 'inn',
                'ogrn', 'registration_date', 'website_url', 'phone', 'email', 'is_rightholder', 'is_partner',
                'is_violator', 'comment', 'payment_details', 'full_address', 'short_address', 'foreign_legal_address',
            )
        }),
        (_('legacy IDs'), {
            # 'classes': ('collapse',),
            'fields': ('legacy_id',),
        }),
        (_('timestamps'), {
            # 'classes': ('collapse',),
            'fields': ('created', 'updated'),
        }),
    )

    def update_from_dadata(self, request: WSGIRequest, pk, inn: str) -> HttpResponse:
        success(request, _('Company was updated successfully. {}'.format(inn)))
        url = reverse('admin:{app}_{model}_change'.format(app=self.model._meta.app_label, model='company'), args=(pk,))
        return HttpResponseRedirect(url)

    def get_urls(self) -> List:
        return [
            url(r'^(.+)/(.+)/update_from_dadata/$', self.update_from_dadata, name='update_from_dadata'),
        ] + super(CompanyAdmin, self).get_urls()

