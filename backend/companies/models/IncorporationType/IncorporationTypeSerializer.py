from dynamic_rest.serializers import DynamicModelSerializer
from .IncorporationType import IncorporationType


__all__ = ('IncorporationTypeSerializer',)


class IncorporationTypeSerializer(DynamicModelSerializer):
    class Meta:
        model = IncorporationType
        fields = ( 'id', 'name', 'abbreviation', )
