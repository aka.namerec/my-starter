from django.contrib import admin


__all__ = ('IncorporationTypeAdmin',)


class IncorporationTypeAdmin(admin.ModelAdmin):
    list_display = ('id', 'name', 'abbreviation',)
    search_fields = ('abbreviation', 'name',)
