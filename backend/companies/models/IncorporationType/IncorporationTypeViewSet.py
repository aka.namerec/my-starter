from .IncorporationType import IncorporationType
from .IncorporationTypeSerializer import IncorporationTypeSerializer
from dynamic_rest.viewsets import DynamicModelViewSet


__all__ = ('IncorporationTypeViewSet',)


class IncorporationTypeViewSet(DynamicModelViewSet):
    queryset = IncorporationType.objects.all()
    serializer_class = IncorporationTypeSerializer
