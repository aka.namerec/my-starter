from django.db import models
from django.utils.translation import ugettext_lazy as _
from common.models import AbstractBaseModel
from common.mixins import LegacyIdMixin


__all__ = ('IncorporationType',)


class IncorporationType(AbstractBaseModel, LegacyIdMixin):
    class Meta:
        verbose_name = _('incorporation type')
        verbose_name_plural = _('incorporation types')

    name = models.CharField(max_length=255, verbose_name=_('name'))
    abbreviation = models.CharField(max_length=255, verbose_name=_('abbreviation'))

    def __str__(self):
        return self.abbreviation or self.name
