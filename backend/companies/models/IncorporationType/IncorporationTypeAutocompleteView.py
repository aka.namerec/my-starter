from .IncorporationType import IncorporationType
from dal import autocomplete


__all__ = ('IncorporationTypeAutocompleteView',)


class IncorporationTypeAutocompleteView(autocomplete.Select2QuerySetView):
    def get_queryset(self):
        qs = IncorporationType.objects.all()

        if self.q:
            qs = qs.filter(name__istartswith=self.q)

        return qs
