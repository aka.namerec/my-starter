from .Person import Person
from dal import autocomplete


__all__ = ('PersonAutocompleteView',)


class PersonAutocompleteView(autocomplete.Select2QuerySetView):
    def get_queryset(self):
        qs = Person.objects.all()
        if self.q:
            qs = qs.filter(last_name__istartswith=self.q)
        return qs
