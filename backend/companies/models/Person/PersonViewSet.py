from .Person import Person
from .PersonSerializer import PersonSerializer
from rest_framework.viewsets import ReadOnlyModelViewSet


__all__ = ('PersonViewSet',)


class PersonViewSet(ReadOnlyModelViewSet):
    queryset = Person.objects.all()
    serializer_class = PersonSerializer
    search_fields = (
        'last_name', 'first_name', 'middle_name', 'maiden_name',
        'birth_date', 'birth_place',
        'inn', 'passport_number', 'phone', 'email',)
    ordering_fields = ('id', 'last_name', 'firest_name', 'middle_name',
        'birth_date', 'birth_place',)
    ordering = ('last_name', 'first_name', 'middle_name',)
