from django.db import models
from django.utils.translation import ugettext_lazy as _
from common.models import AbstractBaseModel
from common.mixins import LegacyIdMixin


__all__ = ('Person',)


class Person(AbstractBaseModel, LegacyIdMixin):
    class Meta:
        verbose_name = _('person')
        verbose_name_plural = _('persons')

    last_name = models.CharField(max_length=255, verbose_name=_('surname'))
    first_name = models.CharField(max_length=255, verbose_name=_('first name'), blank=True, null=True)
    middle_name = models.CharField(max_length=255, verbose_name=_('patronymic'), blank=True, null=True)
    maiden_name = models.CharField(max_length=255, verbose_name=_('maiden name'), blank=True, null=True)

    birth_date = models.DateField(verbose_name=_('birth date'), null=True, blank=True)
    birth_place = models.CharField(max_length=255, verbose_name=_('birth place'), blank=True, null=True)

    inn = models.CharField(max_length=32, verbose_name=_('INN'), unique=True, blank=True, null=True)
    passport_number = models.CharField(max_length=32, verbose_name=_('passport number'), unique=True,
        blank=True, null=True)

    phone = models.CharField(max_length=255, verbose_name=_('phone'), blank=True, null=True)
    email = models.EmailField(max_length=255, verbose_name=_('email'), blank=True, null=True)

    comment = models.TextField(verbose_name=_('comment'), null=True, blank=True)

    diploma = models.FileField(max_length=255, verbose_name=_('diploma'), upload_to='diplomas/',
        null=True, blank=True)

    def __str__(self):
        return ("%s %s" % (
            self.full_name or '',
            self.inn or '',
        )).strip()

    def get_absolute_url(self):
        return '/ui/plainter/persons/%d/' % (self.id)

    def get_edit_url(self):
        return self.get_absolute_url() + 'edit/'

    @property
    def full_name(self):
        return ("%s %s %s" % (
            self.full_last_name or '',
            self.first_name or '',
            self.middle_name or '',
        )).strip()

    @property
    def full_last_name(self):
        if self.last_name:
            if self.maiden_name:
                return "%s (%s)" % (self.last_name, self.maiden_name)
            return self.last_name
        elif self.maiden_name:
            return self.maiden_name

    def save(self, *args, **kwargs):
        super().save(*args, **kwargs)
        try:
            self.individual_enterprise.save()
        except self._meta.model.individual_enterprise.RelatedObjectDoesNotExist:
            pass
