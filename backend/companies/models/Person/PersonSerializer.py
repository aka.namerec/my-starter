from .Person import Person
from rest_framework.serializers import ModelSerializer


__all__ = ('PersonSerializer',)


class PersonSerializer(ModelSerializer):
    class Meta:
        model = Person
        fields = (
            'id',
            'last_name', 'first_name', 'middle_name', 'maiden_name',
            'birth_date', 'birth_place',
            'inn', 'passport_number',
            'phone', 'email', 'comment',
        )
