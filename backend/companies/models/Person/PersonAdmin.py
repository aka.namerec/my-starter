from django.contrib.admin import ModelAdmin


__all__ = ('PersonAdmin',)


class PersonAdmin(ModelAdmin):
    list_display = ('id', 'last_name', 'first_name', 'middle_name', 'maiden_name',
        'birth_date', 'birth_place',
        'inn', 'passport_number',
        'phone', 'email', 'comment',)
    search_fields = ('last_name', 'first_name', 'middle_name', 'maiden_name',
        'inn', 'passport_number',
        'phone', 'email', 'comment',)
    list_filter = ('birth_date', 'birth_place',)
