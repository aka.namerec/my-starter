from .Position import Position
from .PositionSerializer import PositionSerializer
from rest_framework.viewsets import ReadOnlyModelViewSet


__all__ = ('PositionViewSet',)


class PositionViewSet(ReadOnlyModelViewSet):
    queryset = Position.objects.all()
    serializer_class = PositionSerializer
    search_fields = ('name',)
    ordering_fields = ('id', 'name',)
