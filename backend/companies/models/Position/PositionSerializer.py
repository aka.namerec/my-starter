from .Position import Position
from rest_framework.serializers import ModelSerializer


__all__ = ('PositionSerializer',)


class PositionSerializer(ModelSerializer):
    class Meta:
        model = Position
        fields = ('id', 'name',)
