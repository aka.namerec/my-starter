from .Position import Position
from dal.autocomplete import Select2QuerySetView


__all__ = ('PositionAutocompleteView',)


class PositionAutocompleteView(autocomplete.Select2QuerySetView):
    def get_queryset(self):
        qs = Position.objects.all()
        if self.q:
            qs = qs.filter(name__istartswith=self.q)
        return qs
