from django.db import models
from django.utils.translation import ugettext_lazy as _
from common.models import AbstractBaseModel


__all__ = ('Position',)


class Position(AbstractBaseModel):
    class Meta:
        verbose_name = _('position')
        verbose_name_plural = _('positions')

    name = models.CharField(max_length=255, verbose_name=_('name'))

    def __str__(self):
        return self.name
