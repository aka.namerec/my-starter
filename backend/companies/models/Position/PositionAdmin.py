from django.contrib.admin import ModelAdmin


__all__ = ('PositionAdmin',)


class PositionAdmin(ModelAdmin):
    list_display = ('id', 'name',)
    search_fields = ('abbreviation', 'name',)
