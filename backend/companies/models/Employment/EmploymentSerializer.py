from .Employment import Employment
from rest_framework.serializers import ModelSerializer


__all__ = ('EmploymentSerializer',)


class EmploymentSerializer(serializers.ModelSerializer):
    class Meta:
        model = Employment
        fields = (
            'id',
            'company',
            'person',
            'position',
            'is_chief',
            'phone',
            'email',
            'comment',
        )
