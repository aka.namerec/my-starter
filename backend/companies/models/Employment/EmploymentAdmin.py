from django.contrib import admin
# from ..Company.CompanyAutocompleteFilter import CompanyAutocompleteFilter
# from ..Person.PersonAutocompleteFilter import PersonAutocompleteFilter


__all__ = ('EmploymentAdmin',)


class EmploymentAdmin(admin.ModelAdmin):
    list_display = (
        'id',
        'company',
        'person',
        'position',
        'is_chief',
        'work_phone',
        'work_email',
        'comment',
    )
    # list_filter = (CompanyAutocompleteFilter, PersonAutocompleteFilter, 'is_chief', 'position',)
    list_filter = ('is_chief', 'position',)
    autocomplete_fields = ('company', 'person', 'position',)

    class Media:
        extend = True
