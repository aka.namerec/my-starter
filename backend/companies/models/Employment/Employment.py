from django.db import models
from django.utils.translation import ugettext_lazy as _
from common.models import AbstractBaseModel


__all__ = ('Employment',)


class Employment(AbstractBaseModel):
    class Meta:
        verbose_name = _('employment')
        verbose_name_plural = _('employments')

    company = models.ForeignKey(to='Company', on_delete=models.PROTECT, verbose_name=_('company'))
    person = models.ForeignKey(to='Person', on_delete=models.PROTECT, verbose_name=_('person'))

    position = models.ForeignKey(to='Position', verbose_name=_('position'), on_delete=models.PROTECT, null=True)
    is_chief = models.BooleanField(default=False, verbose_name=_('is chief'))

    work_phone = models.CharField(max_length=255, verbose_name=_('work phone'), null=True, blank=False)
    work_email = models.EmailField(max_length=255, verbose_name=_('work email'), null=True, blank=False)

    comment = models.TextField(verbose_name=_('comment'))

    def __str__(self):
        return "%s, %s, %s" % (self.person, self.company, self.position)
