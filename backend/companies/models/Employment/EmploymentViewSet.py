from rest_framework.viewsets import ReadOnlyModelViewSet
from .Employment import Employment
from .EmploymentSerializer import EmploymentSerializer


__all__ = ('EmploymentViewSet',)


class EmploymentViewSet(ReadOnlyModelViewSet):
    queryset = Employment.objects.all()
    serializer_class = EmploymentSerializer
    filter_fields = ('is_chief', 'company', 'person', 'position',)
    search_fields = ('work_email', 'comment', 'work_phone',)
