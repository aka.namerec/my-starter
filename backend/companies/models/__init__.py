from .Person import Person

from .IncorporationType import IncorporationType

from .Company import Company
from .Company.Rightholder import Rightholder
from .Company.Violator import Violator
from .Company.Partner import Partner

from .Position import Position
from .Employment import Employment