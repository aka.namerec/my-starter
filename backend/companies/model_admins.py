__all__ = (
    'register_model_admins',
    'IncorporationTypeAdmin',
    'PersonAdmin',
    'CompanyAdmin',
    'RightholderAdmin',
    'ViolatorAdmin',
    'PartnerAdmin',
    'PositionAdmin',
    'EmploymentAdmin',
)


from .models.IncorporationType.IncorporationTypeAdmin import IncorporationTypeAdmin
from .models.Person.PersonAdmin import PersonAdmin
from .models.Company.CompanyAdmin import CompanyAdmin
from .models.Company.Rightholder.RightholderAdmin import RightholderAdmin
from .models.Company.Violator.ViolatorAdmin import ViolatorAdmin
from .models.Company.Partner.PartnerAdmin import PartnerAdmin
from .models.Position.PositionAdmin import PositionAdmin
from .models.Employment.EmploymentAdmin import EmploymentAdmin


from .models.IncorporationType import IncorporationType
from .models.Person import Person
from .models.Company import Company
from .models.Company.Rightholder import Rightholder
from .models.Company.Violator import Violator
from .models.Company.Partner import Partner
from .models.Position import Position
from .models.Employment import Employment


def register_model_admins (site):
    site.register(IncorporationType, IncorporationTypeAdmin)
    site.register(Person, PersonAdmin)
    site.register(Company, CompanyAdmin)
    site.register(Rightholder, RightholderAdmin)
    site.register(Violator, ViolatorAdmin)
    site.register(Partner, PartnerAdmin)
    site.register(Position, PositionAdmin)
    site.register(Employment, EmploymentAdmin)
